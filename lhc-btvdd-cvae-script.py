# C-VAE with CNN decoder/encoder
# BG June 2019

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from scipy.stats import norm
import os
import cv2
import pandas as pd
import keras

from keras import backend as K

from keras.layers import Input, Dense, Lambda, Layer, Add, Multiply
from keras.models import Model, Sequential
from keras.datasets import mnist
from keras import regularizers
from sklearn.model_selection import train_test_split

import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from scipy.stats import norm

import keras
from keras import layers
from keras.models import Model
from keras import metrics
from keras import backend as K   # 'generic' backend so code works with either tensorflow or theano

K.clear_session()

np.random.seed(237)

imgdir = "../input/btvdds/BTVDDs"

img_dim = 128
crop = 10

def rgb2gray(rgba, img_dim, crop):
    img = rgba[crop:-crop, crop:-crop]
    img = cv2.resize(img, dsize=(img_dim, img_dim),interpolation=cv2.INTER_CUBIC)
    
    img = np.dot(img[...,:3], [0.2989, 0.5870, 0.1140])
    return img

num_images = len(os.listdir(imgdir))
raw_x = np.zeros((num_images, img_dim*img_dim))
class_dict={'Nominal': 0, '0MKBH_0MKBV': 1, 
            '0MKBH_6MKBV': 2, '4MKBH_0MKBV': 3, '3MKBH_6MKBV': 4, '4MKBH_5MKBV': 5}

num_classes = len(class_dict)

y = np.zeros((num_images))
picarg = 0
for i,fn in enumerate(os.listdir(imgdir)):
    img = mpimg.imread(os.path.join(imgdir,fn))    
    img = rgb2gray(img, img_dim, crop)
    ##scale between -1 and 1
    #img = 2*(img - np.min(img)) / (np.max(img) - np.min(img)) - 1
    #scale between 0 and 1
    img = (img - np.min(img)) / (np.max(img) - np.min(img))
    fimg = img.flatten()
    raw_x[i,:,] = fimg
    #find categorical
    for key, value in class_dict.items():
        if key in fn:
            y[i] = value
    if y[i] == picarg:
        plt.imshow(img, cmap=plt.get_cmap('gray'))
        plt.show()
        picarg+=1
        
#raw_x = np.random.shuffle(raw_x)

X = raw_x.reshape((raw_x.shape[0], img_dim, img_dim,1))

x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

print(x_train.shape, x_test.shape)

img_shape = (img_dim, img_dim, 1)   
batch_size = 32
latent_dim = 2  # Number of latent dimension parameters
filters=32

# Encoder architecture: Input -> Conv2D*4 -> Flatten -> Dense
input_img = keras.Input(shape=img_shape)

x = layers.Conv2D(filters, 3,
                  padding='same', 
                  activation='relu')(input_img)
x = layers.Conv2D(filters, 3,
                  padding='same', 
                  activation='relu',
                  strides=(2, 2))(x)
x = layers.Conv2D(filters*2, 3,
                  padding='same', 
                  activation='relu')(x)
x = layers.Conv2D(filters*2, 3,
                  padding='same', 
                  activation='relu',
                  strides=(2, 2))(x)
x = layers.Conv2D(filters*4, 3,
                  padding='same', 
                  activation='relu')(x)
x = layers.Conv2D(filters*4, 3,
                  padding='same', 
                  activation='relu',
                  strides=(2, 2))(x)
x = layers.Conv2D(filters*8, 3,
                  padding='same', 
                  activation='relu')(x)
# need to know the shape of the network here for the decoder
shape_before_flattening = K.int_shape(x)

x = layers.Flatten()(x)
x = layers.Dense(32, activation='relu')(x)

# Two outputs, latent mean and (log)variance
z_mu = layers.Dense(latent_dim)(x)
z_log_sigma = layers.Dense(latent_dim)(x)

# sampling function
def sampling(args):
    z_mu, z_log_sigma = args
    epsilon = K.random_normal(shape=(K.shape(z_mu)[0], latent_dim),
                              mean=0., stddev=1.)
    return z_mu + K.exp(z_log_sigma) * epsilon

# sample vector from the latent distribution
z = layers.Lambda(sampling)([z_mu, z_log_sigma])

# decoder takes the latent distribution sample as input
decoder_input = layers.Input(K.int_shape(z)[1:])

# Expand to img_dim**2 total pixels
x = layers.Dense(np.prod(shape_before_flattening[1:]),
                 activation='relu')(decoder_input)

# reshape
x = layers.Reshape(shape_before_flattening[1:])(x)

# use Conv2DTranspose to reverse the conv layers from the encoder
x = layers.Conv2DTranspose(filters, 3,
                           padding='same', 
                           activation='relu',
                           strides=(2, 2))(x)
x = layers.Conv2DTranspose(filters*2, 3,
                           padding='same', 
                           activation='relu',
                           strides=(2, 2))(x)
x = layers.Conv2DTranspose(filters*4, 3,
                           padding='same', 
                           activation='relu',
                           strides=(2, 2))(x)
x = layers.Conv2D(1, 3,
                  padding='same', 
                  activation='sigmoid')(x)

# decoder model statement
decoder = Model(decoder_input, x)
print(decoder.summary())

# apply the decoder to the sample from the latent distribution
z_decoded = decoder(z)

# construct a custom layer to calculate the loss
class CustomVariationalLayer(keras.layers.Layer):

    def vae_loss(self, x, z_decoded):
        x = K.flatten(x)
        z_decoded = K.flatten(z_decoded)
        # Reconstruction loss
        xent_loss = keras.metrics.binary_crossentropy(x, z_decoded)
        # KL divergence
        kl_loss = -5e-4 * K.mean(1 + z_log_sigma - K.square(z_mu) - K.exp(z_log_sigma), axis=-1)
        return K.mean(xent_loss + kl_loss)

    # adds the custom loss to the class
    def call(self, inputs):
        x = inputs[0]
        z_decoded = inputs[1]
        loss = self.vae_loss(x, z_decoded)
        self.add_loss(loss, inputs=inputs)
        return x

# apply the custom loss to the input images and the decoded latent distribution sample
y = CustomVariationalLayer()([input_img, z_decoded])

# VAE model statement
vae = Model(input_img, y)
vae.compile(optimizer='rmsprop', loss=None)
print(vae.summary())

# fit the VAE
hist = vae.fit(x=x_train, y=None,
        shuffle=True,
        epochs=20,
        batch_size=batch_size,
        validation_data=(x_test, None))
        
# summarize history for loss
print(len(hist.history['loss']))
plt.plot(hist.history['loss'][1:])
plt.plot(hist.history['val_loss'][1:])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# display a 2D plot of the digit classes in the latent space
encoder = Model(input_img, z_mu)
z_test = encoder.predict(x_test, batch_size=batch_size)
plt.figure(figsize=(6, 6))
plt.scatter(z_test[:, 0], z_test[:, 1], c=y_test,
            alpha=.4, cmap='viridis')
plt.xlabel('Z_test_0')
plt.ylabel('Z_test_1')
plt.colorbar()
plt.show()

# display a 2D manifold of the images
n = 10  # figure with nxn images
digit_size = img_dim

# linearly spaced coordinates on the unit square were transformed
# through the inverse CDF (ppf) of the Gaussian to produce values
# of the latent variables z, since the prior of the latent space
# is Gaussian
d = 0.001
u_grid = np.dstack(np.meshgrid(np.linspace(d, 1-d, n),
                               np.linspace(d, 1-d, n)))
z_grid = norm.ppf(u_grid)
x_decoded = decoder.predict(z_grid.reshape(n*n, 2))
x_decoded = x_decoded.reshape(n, n, digit_size, digit_size)

fig, ax = plt.subplots(figsize=(12, 12))
ax.imshow(np.block(list(map(list, x_decoded))), 
cmap='gray', interpolation='none', 
extent=[np.min(z_grid),np.max(z_grid),np.max(z_grid),np.min(z_grid)])
ax.set_aspect(1)
plt.xlabel('Z_test_0')
plt.ylabel('Z_test_1')

# plt.figure(figsize=(12, 12))

# plt.imshow(np.block(list(map(list, x_decoded))), cmap='gray')
plt.show()
